# Graph–Based Plagarism Detection of ER Schemas and SQL DDL Code
LaTeX repository for the *Graph–Based Plagarism Detection of ER Schemas and SQL DDL Code* academic article.

## Compiling
To compile the project into a PDF file, run

```sh
pdflatex DatabasePlagiarism.tex
```
