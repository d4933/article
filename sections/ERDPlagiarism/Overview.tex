\subsection{Similarity flooding algorithm}
The \emph{similarity flooding algorithm} is a method for evaluating the similarity of two data structures, referred to as \emph{models}, that can be converted into directed, labeled graphs. 

The central idea underlying this method is to \emph{consider two nodes $a$ and $b$ as similar if they are referenced by similar nodes}.
The similarity value that is computed is a node-by-node one, that is, a similarity score is assigned between a pair of any two nodes from the two graphs, rather than to the graphs themselves.

\subsubsection{Calculation procedure}
To give an outline of the calculation procedure, take a pair of models. In order to compute their similarity, we

\begin{enumerate}
    \item Convert both models into two graphs $A = (V_A, E_A)$ and $B = (V_B, E_B)$. Any edge in $A$ or $B$ is represented as a triple $(a, l, b)$, where $a$ and $b$ are the source and target nodes for the edge, and $p$ is its label
    \item Estimate an initial similarity score for every $(a, b) \in A \times B$
    \item Iteratively apply a fix-point procedure to refine the similarity values
    \item Filter the results
\end{enumerate}

The evaluation of the first similarity values of point 2 can be performed by using domain-specific information, or by computing a string-based distance between the nodes of the two graphs.

The fix-point procedure from point 3 propagates the initial similarity scores throughout the node pairs until some convergence criteria are met. At each iteration, the similarity scores are normalized, so that they fall in the $[0, 1]$ range.

More precisely, a \emph{coefficients propagation map} $w: (A \times B) \times (A \times B) \to [0, 1]$ is built during this phase, encoding the spreading out of similarity values from node pairs to node pairs.
That is, if $(a, l, a') \in E_A$ and $(b, l, b') \in E_B$, and the pair $(a, b)$ is recognized to be similar, then the similarity of this pair is distributed to the pair $(a', b')$ as $w\left( (a, b), (a', b') \right)$. Note that only pairs of edges from the two graphs, sharing the same label $l$, are considered. 

\subimport{Overview/}{Models}

As an example, consider the pair of graphs from \figref{fig.graph.example}. Since their edge labels differ, no similarity is spread from $(a, b)$ to $(a_2, b_2)$. However, $(a, l_1, a_1) \in E_A$ and $(b, l_1, b_1) \in E_B$, so the pair $(a, b)$ contributes a fraction of $w\left( (a, b), (a_1, b_1) \right)$ to the similarity of $(a_1, b_1)$.

The similarity between a pair $(a, b) \in A \times B$ is expressed as a \emph{similarity map} $\sigma: A \times B \to [0, 1]$. Denote by $\sigma^i$ the similarity map computed after the $i$th iteration, so that $\sigma^0$ represents the base map constructed in step 2. Then we can express the iterative computation $\sigma^{i + 1}$ in terms $\sigma^i$ as

\begin{align}
    & \sigma^{i + 1} (a, b) = \sigma^i (a, b) \nonumber \\
    & + \sum_{
	\substack{
	    (a', l, a) \in A \\
	    (b', l, b) \in B 
	}
    } w \left( (a', b'), (a, b) \right) \cdot \sigma^i(a', b') \\
    & + \sum_{
	\substack{
	    (a, l, a') \in A \\
	    (b, l, b') \in B 
	}
    } w \left( (a', b'), (a, b) \right) \cdot \sigma^i(a', b') \nonumber
\end{align}

where normalization is omitted for simplicity. That is, if nodes $a$ and $b$ both have two in-neighbours or two out-neighbours $a'$ and $b'$ sharing the same label $l$, then the current similarity $\sigma^i(a', b')$ of these neighbours, times $w\left( (a', b'), (a, b) \right)$, is added to that of $a$ and $b$.

This iteration proceeds until the euclidean norm of the residual vector $\Delta(\sigma^{i + 1}, \sigma^i)$ becomes less than a specified $\varepsilon$ for some $i$, or after a maximum number of iterations is performed.

The similarity map $\sigma$ thus produced is fed into step 4, where some filtering rules are used to reduce its size, i.e., the number of $(a, b)$ pairs such that $(a, b) \in \mathrm{dom} ~ \sigma$. Selection techniques developed for the matching of bipartite graphs can also be used, to determine the optimal mapping to be delivered to the user.
