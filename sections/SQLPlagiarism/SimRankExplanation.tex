\subsection{SimRank}
SimRank is a similarity algorithm for graphs. Contrasting with the similarity flooding method, the similarity of SimRank is computed between nodes of the \emph{same} graph.
For the case of SimRank, however, limiting the similarity to just nodes does not preclude from extending the similarity to whole SQL code fragments, since, as we will see in section \ref{section.simrank.sql}, two such fragments can be always encoded as nodes of the graph.
The similarity of SimRank depends on the structural context alone. Domain-specific similarities, however, can be specified by setting a-priori values. 

To understand the functioning of SimRank on an intuitive level, consider the graph of \figref{fig.simrank.example}. The general rule of similarity for SimRank is to \emph{regard two nodes as similar if they are referenced by similar nodes}; as an edge case, each node is maximally similar to itself.
In \figref{fig.simrank.example}, nodes \texttt{2} and \texttt{3} are both connected by node \texttt{1}. Since node \texttt{1} is maximally similar to itself, nodes \texttt{2} and \texttt{3} will tend to be regarded as somehow similar too. Their similarity will then spread to nodes \texttt{4} and \texttt{5} and, ultimately, to other nodes in the graph.

\subimport{SimRankExplanation/}{ExampleGraph}

More formally, let $G = (V, E)$ be a graph, where $V$ represents the set of objects to be compared, and $E \subseteq V \times V$ the set of relationships among them. Given a node $v \in V$, we denote by $I(v)$ the set of in-neighbours of $v$, and by $O(v)$ the set of out-neighbours of $v$. Individual in-neighbours and out-neighbours of $v$ are indicated by $I_i(v)$ and $O_j(v)$, respectively, where $1 \leq i \leq |I(v)|$ and $1 \leq j \leq |O(v)|$.

In the base case, a node $v$ is similar (identical) to itself. Other base similarity values can be established by an explicit assignment, depending for example by the similarity of two pairs of user-provided text.

Denote the similarity of two vertices $a, b$ as $s(a, b) \in [0, 1]$. As we just said, if $a = b$, then $s(a, b) = 1$. Otherwise

\begin{align}
    s(a, b) = \frac{C}{|I(a)| |I(b)|} \sum_{i = 1}^{|I(a)|} \sum_{j = 1}^{|I(b)|} s\left( I_i(a), I_j(b) \right)
    \label{eq.simrank}
\end{align}

where $C$ is a constant between $0$ and $1$, that can be thought of as a confidence level or a decay factor\footnotemark.
Equation \eqref{eq.simrank} states that, in order to compute the similarity between $a$ and $b$, \emph{we need to iterate over all in-neighbours of $a$ and $b$, respectively, and sum up the similarity of each pair}. The division by $|I(a)| |I(b)|$ acts as a normalization factor.

What was said thus far was intended to transmit only a quick understanding of the main ideas behind SimRank; to know more, consult \cite{Glen2002}. We can now describe the adaptation of SimRank to the similarity detection of SQL DDL code.

\footnotetext{
    For more details into this constant and other aspects of the algorithm, refer to \cite{Glen2002}.
}
