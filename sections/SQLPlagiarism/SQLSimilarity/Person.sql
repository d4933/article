CREATE TABLE Person (
	Id INTEGER PRIMARY KEY,
	Name VARCHAR (50),
	MiddleName VARCHAR (50),
	LastName VARCHAR (50),
	Birth DATE,
	BirthNation VARCHAR(50)
)
