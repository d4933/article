# Article Outline

## Abstract

## Introduction

- [x] Add a "The main contributions of this paper are" paragraph

## Known work

- [x] Mention "Detection of Plagiarism in Database Schemas Using Structural Fingerprints"
- [x] Scerbakov, Schukin and Sabinin proposal from University of Gratz
- [x] Gordon Russel attempt from Napier university
- [x] Niger Stanger article
- [x] "Matching UML class models using graph edit distance" (not sure whether to include it)
- [x] Unfeasibility of GED (relate this to the previous article)
- [x] Mentioning of neural network and machine learning techniques
- [x] Observation that NN or ML techniques require training time or big datasets
- [x] Consider mentioning "A survey of graph edit distance"

## Plagiarism detection of ER diagrams

- [x] Give brief overview of ER algorithm

### Overview of Similarity Flooding algorithm

- [x] Translation of an information piece into graph
- [x] Initial computation of similarity scores
- [x] Application of similarity flooding algorithm
- [x] Filtering of results

## Plagiarism detection of SQL code

- [x] Give brief overview of SimRank
- [ ] Consider indexes

## Conclusion and future work

- [x] Simplify ERD graph conversion for the SF algorithm
- [x] SimRank for ERD similarity detection
- [x] Assess and improve results of SF and SimRank
- [x] Extension to DML statements

### Minor points

- [ ] Better pictures with TikZ
